---
author: "Krishna Bhogaonker"
title: "Discussion 2 - Simulate 2 Population Model"
date: "2021-04-02"
---



# Goal

Hello everyone. Here is my updated post. 

To understand the gender diversity question, we need to understand the interation 
of two different populations. Models such as these are common in both biology 
and chemistry. In biology, population models are used to study the growth of 
a species, as well as the evolution of competing species. The well known 
Lokta-Volterra equation is often referred to by its common name, the "predator-prey" 
equation. 

$$
\hat{y} = W \odot f(x, y)
$$

Scholars in chemistry and chemical engineering face an analoguous problem 
to biologist, except in this case the focus is on chemical species instead of 
biological species. Chemists often run experiments where different chemical 
species may result from a reaction. Hence the chemist must control the reaction 
to generate as much of the desired output as possible, while minimizing the 
unwanted outputs. 

The goal of both chemists and biologists is to control the size of each population, 
given some criterion. In the case of a biologist the goal is some level of 
balance in the ecosystem, given some definition of that balance. In the case of 
chemists, the focus may be on maximizing one species while minimizing the other 
species, given a cost constraint. 

We can define the dynamics of any population model using systems of differential 
equations. And as with any system of differential equations, we can apply a 
driving force to modulate the behavior of the system. Consider a simple system 
where we have a 10 kg weight hanging on a spring. If we whack the weight 
once, the spring will move downward and then bound upward over time.

Insert picture. 

```julia
x = 1:10; y = rand(10, 2)
plot(x, y)
```

{{< figure src="../figures/discussion2/discussion_2_simulate_2_populations_2_1.png"  >}}


However, if we continue to apply force on the weight or if we bounce around the 
pedestal on which the spring is mounted, then we can actually drive the force to 
much farther locations in its range of motion. If we are not careful, we can 
actually break the spring from the force. 

So say that we wanted to drive this spring to a particular location in the 
domain. Or perhaps we want to keep that spring at a certain location in the domain 
so that it does not move. Think about a shock absorber on a car. We definitely 
do not want to wheel to lose contact with the road, but if there are bumps in the 
road or inclines then declines, the car may jump. Sock absorbers prevent excessive 
shocks on the wheel from either direction. 

Insert picture

In the case of organizational behavior and the gender balance of faculty within 
an academic department, we have a similar issue of competing populations: men
and women professors. In this case we do not need to worry about one population 
eating the other, as in the original wolves and rabbits case. Instead we 
worry about faculty entering and existing an academic department based upon 
some hiring and attrition rates. Further, the faculty who are in the department 
are the ones who decide who is hired into the department.

So we need to understand how biased decision making works. As a simple 
model, we can think of people evaluating the profile of a candidate from 
a different level of bias. And we don't need that much bias to get a 
problem. So we can see how as we change the distribution, we would get more 
and more bias in the results. This is easy to produce. 

I should have a picture of a single random walk. And then the difference of 
two random walks. 

But the goal was to understand how to control the replacement of people who 
leave. That is a tricky point. So if we assume that there is a process of 
people leaving and people joining, this gives us a model. There is no necessary 
reason for an oscillation here, because 

```julia
using DifferentialEquations
f(u,p,t) = 1.01*u
u0 = 1/2
tspan = (0.0,1.0)
prob = ODEProblem(f,u0,tspan)
sol = solve(prob, Tsit5(), reltol=1e-8, abstol=1e-8)

using Plots
plot(sol,linewidth=5,title="Solution to the linear ODE with a thick line",
     xaxis="Time (t)",yaxis="u(t) (in μm)",label="My Thick Line!") # legend=false
plot!(sol.t, t->0.5*exp(1.01t),lw=3,ls=:dash,label="True Solution!")
```

{{< figure src="../figures/discussion2/discussion_2_simulate_2_populations_3_1.png"  >}}
